#include <stdio.h>
#include <assert.h>
#include "opus/opus.h"

int main()
{
#define FRAME_SIZE  80
#define BUFFER_SIZE (FRAME_SIZE * 2)
    int             error; 
    OpusEncoder    *encoder; 
    OpusDecoder    *decoder; 
    FILE           *read_ptr; 
    FILE           *write_ptr; 
    unsigned char   input[BUFFER_SIZE]; 
    unsigned char   output[BUFFER_SIZE]; 
    unsigned char   to_file[BUFFER_SIZE]; 
    int             bytes_encoded; 
    int             samples_decoded; 
    unsigned int    bytes_read; 
    unsigned int    iter = 0; 


    read_ptr = fopen("8khz_1ch_s16.raw", "r"); 
    write_ptr = fopen("opus_decoded.raw", "w"); 

    encoder = opus_encoder_create(8000, 1, OPUS_APPLICATION_VOIP, &error); 
    assert(error == 0); 

    decoder = opus_decoder_create(8000, 1, &error); 
    assert(error == 0); 

    while(1)
    {
        if((iter % 50) != 0)
        {
            bytes_read = fread(input, 1, BUFFER_SIZE, read_ptr); 
            if(bytes_read == 0)
            {
                break; 
            }

            bytes_encoded = opus_encode(encoder, (const opus_int16 *) input, FRAME_SIZE, output, BUFFER_SIZE); 
            assert(bytes_encoded > 0); 

            printf("bytes_read = %d, bytes_encoded = %d\n", bytes_read, bytes_encoded); 

            samples_decoded = opus_decode(decoder, output, bytes_encoded, (opus_int16 *) to_file, FRAME_SIZE, 0); 
        }
        else
        {
            printf("Faking underrun and insert NULL sample...\n"); 

            samples_decoded = opus_decode(decoder, NULL, 0, (opus_int16 *) to_file, FRAME_SIZE, 0); 
        }

        printf("bytes_encoded= %d, samples_decoded = %d\n", 
               bytes_encoded, 
               samples_decoded); 

        assert(samples_decoded > 0); 

        fwrite(to_file, 1, samples_decoded*2, write_ptr); 

        iter++; 
    }

    printf("iterations = %d\n", iter); 

    fflush(write_ptr); 
    fclose(write_ptr); 
    fclose(read_ptr); 
    
    return 0; 
}
